package be.kdg.TestDrivenDevelopment;

import lombok.Data;

@Data
public class AddfavoriteDto {
    private String password;
    private String username;
    private String favorite;

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getFavorite() {
        return favorite;
    }
}
