package be.kdg.TestDrivenDevelopment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    private final FavoriteService favoriteService;

    public LoginController(FavoriteService favoriteService) {
        this.favoriteService = favoriteService;
    }


    @GetMapping("/tdd")
    public ModelAndView tdd() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("tdd");
        return mav;
    }

    @PostMapping("/tdd")
    public ModelAndView postTdd(@ModelAttribute User user) {
        if (favoriteService.checkLogin(user.getUsername(), user.getPassword())) {
            return loggedin(user);
        }
        return new ModelAndView("tdd", "error", 1);

    }

    @GetMapping("/loggedin")
    public ModelAndView loggedin(User user) {
        ModelAndView mav = new ModelAndView("loggedin", "username", "Favorites: " + user.getUsername());
        mav.addObject("gebruiker", user);
        mav.addObject("favorites", favoriteService.getFavorites(user.getUsername(), user.getPassword()));
        return mav;
    }

    @PostMapping("/addUser")
    public ModelAndView AddUser(@ModelAttribute newUserDto newUser) {

        System.out.println(newUser.getNewusername() + ";" + newUser.getNewpassword());
        favoriteService.addUser("root", "rootpasswd", newUser.getNewusername(), newUser.getNewpassword());
        return loggedin(new User("root", "rootpasswd"));
    }

    @PostMapping("/addFavorite")
    public ModelAndView addFavorite(@ModelAttribute AddfavoriteDto addfavoriteDto){
        System.out.println(addfavoriteDto);
        favoriteService.addFavorite(addfavoriteDto.getUsername(), addfavoriteDto.getPassword(), addfavoriteDto.getFavorite());
        return loggedin(new User(addfavoriteDto.getUsername(),addfavoriteDto.getPassword()));
    }
}
