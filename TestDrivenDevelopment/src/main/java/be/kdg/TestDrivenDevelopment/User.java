package be.kdg.TestDrivenDevelopment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {
    private String username;
    private String password;
    private List<String> favorites;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.favorites = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    public List<String> getFavorites() {
        return favorites;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
