package be.kdg.TestDrivenDevelopment;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserManagement {
    private Set<User> users;


    public UserManagement() {
        this.users = new HashSet<>();
        addUser(new User("root", "rootpasswd"));
    }

    public Set<User> getUsers() {
        return users;
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void removeUser(User user) {
        this.users.remove(user);
    }

    public User getUser(String username, String password) {
        for (User user :
                users) {
            if (user.equals(new User(username, password))){
                return user;
            }
        }
        return null;
    }
}
