package be.kdg.TestDrivenDevelopment;

import lombok.Data;
import lombok.Getter;

@Data
public class newUserDto {

    private String newusername;
    private String newpassword;

    public String getNewusername() {
        return newusername;
    }

    public String getNewpassword() {
        return newpassword;
    }
}
