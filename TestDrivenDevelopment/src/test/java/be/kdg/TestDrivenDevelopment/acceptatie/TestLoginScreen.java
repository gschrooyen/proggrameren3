package be.kdg.TestDrivenDevelopment.acceptatie;

import be.kdg.TestDrivenDevelopment.TestDrivenDevelopmentApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest(classes = {TestDrivenDevelopmentApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TestLoginScreen {
    @Test
    public void testLoginScreen() {
        WebDriver driver = new HtmlUnitDriver();
        driver.get("http://127.0.0.1:8181/tdd");
        String title = driver.getTitle();
        assertEquals("Favorites login", title);
        WebElement element = driver.findElement(By.name("username"));
        String tagName = element.getTagName();
        assertEquals("input", tagName);
        element = driver.findElement(By.name("password"));
        tagName = element.getTagName();
        assertEquals("input", tagName);
        String type = element.getAttribute("type");
        assertEquals("password", type);
        element = driver.findElement(By.name("login"));
        tagName = element.getTagName();
        assertEquals("input", tagName);
        type = element.getAttribute("type");
        assertEquals("submit", type);
    }
}
