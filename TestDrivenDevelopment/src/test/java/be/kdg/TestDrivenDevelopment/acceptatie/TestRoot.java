package be.kdg.TestDrivenDevelopment.acceptatie;

import be.kdg.TestDrivenDevelopment.TestDrivenDevelopmentApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static be.kdg.TestDrivenDevelopment.acceptatie.CommonOperations.loginAsRoot;
import static be.kdg.TestDrivenDevelopment.acceptatie.CommonOperations.loginAsUser;

@SpringBootTest(classes = {TestDrivenDevelopmentApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TestRoot {
    @Test
    public void testAddUser() throws InterruptedException {
        WebDriver driver = new HtmlUnitDriver();
        loginAsRoot(driver);
        WebElement element = driver.findElement(By.name("newusername"));
        element.sendKeys("testuser");
        element = driver.findElement(By.name("newpassword"));
        element.sendKeys("testuser");
        element.submit();
        driver = new HtmlUnitDriver();
        loginAsUser(driver, "testuser", "testuser");
    }
}
