package be.kdg.TestDrivenDevelopment.unit;

import be.kdg.TestDrivenDevelopment.User;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.constraints.AssertTrue;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class UserUnitTests {
    @Test
    public void equals() {
        User usr = new User("glenn", "supersecret");
        User usrWithFavs = new User("glenn", "supersecret");
        usrWithFavs.getFavorites().add("test");
        assertEquals("Users with the same name and password should equal regardless of favorites", usr, usrWithFavs);
    }
}
