package be.kdg.webservice.restClient;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyResource {
    @GET("myresource/{id}")
    Call<Record> helloWorld(@Path("id") String id, @Query("name") String name);
}
