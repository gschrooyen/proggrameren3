package be.kdg.webservice.restClient;

/*
 * Data Transfer Object
 */
public class Record {
    private String name;
    private String id;

    public Record() {
        this.id = "0";
        this.name = "no name";
    }

    public Record(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Record{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
