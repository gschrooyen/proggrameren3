package be.kdg.webservice.restClient;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

public class StartRestClient {
    public static void main(String[] args) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://127.0.0.1:2222/test/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyResource serviceStub = retrofit.create(MyResource.class);
        Call<Record> recordCall = serviceStub.helloWorld("123", "Jos");
        Response<Record> response = recordCall.execute();
        Record record = response.body();
        System.out.println("record = " + record);
    }
}
