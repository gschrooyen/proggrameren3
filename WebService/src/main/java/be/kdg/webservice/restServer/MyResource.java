package be.kdg.webservice.restServer;

import be.kdg.webservice.restClient.Record;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("test")
public class MyResource {
    // surf naar http://127.0.0.1:2222/test/myresource/3015?name=kris
    @GET
    @Path("myresource/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Record helloWorld(@PathParam("id") String id, @QueryParam("name") String name) {
        Record record = new Record(name, id);
        System.out.println(record);
        return record;
    }
}
