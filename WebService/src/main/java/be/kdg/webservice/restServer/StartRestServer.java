package be.kdg.webservice.restServer;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

public class StartRestServer {
    public static void main(String[] args) {
        ResourceConfig config = new ResourceConfig();
        config.packages("be.kdg.webservice.restServer");
        ServletHolder skeletonServlet = new ServletHolder(new ServletContainer(config));
        skeletonServlet.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true");

        Server server = new Server(2222);
        ServletContextHandler context = new ServletContextHandler(server, "/*");
        context.addServlet(skeletonServlet, "/*");

        try {
            server.start();
            server.join();
        } catch (Exception e){
            server.destroy();
        }
    }
}
