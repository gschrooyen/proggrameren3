import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

import java.util.UUID;

/**
 * @author Shawn
 */
public class ClientSkeleton {

    private final ChatServer sender;
    private final MessageManager messageManager;
    private final String id;
    private final ChatClient client;
    private ChatFrame frame;

    /**
     * Sends reply with no return-value to the originator of a request.
     *
     * @param request the request that is aswered.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    /**
     * Creates a new Client component.
     *
     * @param contactsAddress the address of the Contacts component.
     */
    public ClientSkeleton(NetworkAddress contactsAddress, String name) {
        this.id = UUID.randomUUID().toString();
        this.messageManager = new MessageManager();
        System.out.println(messageManager.getMyAddress());
        sender = new ChatServerStub(contactsAddress, messageManager.getMyAddress());
        this.client = new ChatClient(sender, name);
    }

    /**
     * Adds two contacts (1 invalid and one correct) to the Contacts component
     * and tests if they are present.
     */
    private void run() {
        frame = new ChatFrame(client);

        while(true) {
            MethodCallMessage message = messageManager.wReceive();
            handleRequest(message);
        }
    }

    private void handleRequest(MethodCallMessage request) {
        System.out.println(request);
        if ("receive".equalsIgnoreCase(request.getMethodName())) {
            System.out.println(request.getParameter("text"));
            client.receive(request.getParameter("text"));
            sendEmptyReply(request);
            System.out.println("Sending empty reply");
        } else if ("getname".equalsIgnoreCase(request.getMethodName())) {
            handleGetName(request);
        }

    }

    private void handleGetName(MethodCallMessage request) {
        System.out.println("Getting name");
        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), "getname");
        msg.setParameter("name", client.getName());
        messageManager.send(msg, request.getOriginator());
        System.out.println("Sent get name");
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            System.err.println("Usage: java ContactsSkeleton <chatServerIP> <chatServerPort> <clientName>");
            System.exit(1);
        }
        int port = Integer.parseInt(args[1]);
        NetworkAddress contactsAddress = new NetworkAddress(args[0], port);
        ClientSkeleton clientSkeleton = new ClientSkeleton(contactsAddress, args[2]);
        clientSkeleton.run();
    }
}
