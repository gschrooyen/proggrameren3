package be.kdg;

/**
 * @author Shawn
 */
public interface ChatServer {
    void send(String name, String text);
    void register(ChatClient client);
    void unregister(ChatClient client);
}
