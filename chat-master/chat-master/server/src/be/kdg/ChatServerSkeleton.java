package be.kdg;

import be.kdg.communication.MessageManager;
import be.kdg.communication.MethodCallMessage;
import be.kdg.communication.NetworkAddress;

/**
 * @author Shawn
 */
public class ChatServerSkeleton {
    private final MessageManager manager;
    private final ChatServer sender;

    public ChatServerSkeleton() {
        this.manager = new MessageManager(50505);
        System.out.println(manager.getMyAddress());
        this.sender = new ChatServerImpl();
    }

    /**
     * Sends reply with no return-value to the originator of a request.
     *
     * @param request the request that is aswered.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(manager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        manager.send(reply, request.getOriginator());
    }

    /**
     * Handles an incomming request.
     *
     * @param request the request that is being handled.
     */
    private void handleRequest(MethodCallMessage request) {
        //System.out.println("ContactsSkeleton:handleRequest(" + request + ")");
        System.out.println(request);
        String methodName = request.getMethodName();
        if ("send".equals(methodName)) {
            handleSend(request);
        } else if ("register".equalsIgnoreCase(methodName)) {
            handleRegister(request);
        } else if ("unregister".equalsIgnoreCase(methodName)) {
            handleUnregister(request);
        }
        else {
            System.out.println("ContactsSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    private void handleSend(MethodCallMessage request) {
        String name = request.getParameter("name");
        String text = request.getParameter("text");
        sender.send(name, text);
        sendEmptyReply(request);
    }

    private void handleRegister(MethodCallMessage request) {
        String[] address = request.getParameter("skeleton").split(":");
        int port = Integer.parseInt(address[1]);
        String host = address[0];
        NetworkAddress networkAddress = new NetworkAddress(host, port);
        ChatClient client = new ChatClientStub(networkAddress);
        sender.register(client);
        sendEmptyReply(request);
    }

    private void handleUnregister(MethodCallMessage request) {
        String[] address = request.getParameter("skeleton").split(":");
        int port = Integer.parseInt(address[1]);
        String host = address[0];
        NetworkAddress networkAddress = new NetworkAddress(host, port);
        ChatClient client = new ChatClientStub(networkAddress);
        sender.unregister(client);
        System.out.println("unregistring");
        sendEmptyReply(request);
    }


    private void run() {
        while (true) {
            MethodCallMessage message = manager.wReceive();
            handleRequest(message);
        }
    }
    public static void main(String[] args) {
        ChatServerSkeleton chatServerSkeleton = new ChatServerSkeleton();
        chatServerSkeleton.run();
    }
}
