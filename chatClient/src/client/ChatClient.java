package client;

public final class ChatClient {
    private final String naam;
    private TextReceiver receiver;

    public ChatClient(String naam) {
        this.naam = naam;
    }

    public String getNaam() {
        return naam;
    }

    public void setReceiver(TextReceiver receiver) {
        this.receiver = receiver;
    }

    public void recieve(String message){
        if (this.receiver == null) return;
        receiver.receive(message);
    }
}
