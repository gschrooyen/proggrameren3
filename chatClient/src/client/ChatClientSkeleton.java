package client;

import communication.MessageManager;
import communication.MethodCallMessage;

public class ChatClientSkeleton {
    private final MessageManager messageManager;
    private final ChatClients chatClients;

    public ChatClientSkeleton() {
        chatClients = new ChatClientImpl();
        messageManager = new MessageManager();
        System.out.println("my address = " + messageManager.getMyAddress());
    }

    /**
     * Sends reply with no return-value to the originator of a request.
     *
     * @param request the request that is being handled.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleReceive(MethodCallMessage request){
        String message = request.getParameter("message");
        chatClients.receive(message);
        sendEmptyReply(request);
    }

    private void handleRequest(MethodCallMessage request) {
        //System.out.println("ZipCodesSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("recieve".equals(methodName)) {
            handleReceive(request);
        } else {
            System.out.println("ContactsSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    private void run() {
        while (true){
            MethodCallMessage req = messageManager.wReceive();
            handleRequest(req);
        }
    }

    public static void main(String[] args) {
        ChatClientSkeleton chatClientSkeleton = new ChatClientSkeleton();
        chatClientSkeleton.run();
    }
}
