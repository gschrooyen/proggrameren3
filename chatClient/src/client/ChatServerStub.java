package client;

import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public class ChatServerStub {
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public ChatServerStub(NetworkAddress serverAddress) {
        this.serverAddress = serverAddress;
        messageManager = new MessageManager();
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    public void register(){
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "register");
        message.setParameter("ipAddress", messageManager.getMyAddress().getIpAddress());
        message.setParameter("port", String.valueOf(messageManager.getMyAddress().getPortNumber()));
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    public void unRegister(){
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "unRegeister");
        message.setParameter("ipAddress", messageManager.getMyAddress().getIpAddress());
        message.setParameter("port", String.valueOf(messageManager.getMyAddress().getPortNumber()));
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    private void send(String msg, String name){
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "send");
        message.setParameter("name", name);
        message.setParameter("message", msg);
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }
}
