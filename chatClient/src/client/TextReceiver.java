package client;

public interface TextReceiver {
    void receive(String text);
}
