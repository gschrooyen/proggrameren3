package server;

import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

import java.util.HashMap;
import java.util.Map;

public class ChatClientStub implements ChatClients{
    private final NetworkAddress clientAddress;
    private final MessageManager messageManager;

    public ChatClientStub(NetworkAddress clientAddress) {
        this.clientAddress = clientAddress;
        this.messageManager = new MessageManager();
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    public void sendMessage(String message) {
        Map<String, String> params = new HashMap<>();
        params.put("message", message);
        MethodCallMessage methodCallMessage = new MethodCallMessage(clientAddress,"send", params );
    }

    public String getName() {
        return null;
    }
}
