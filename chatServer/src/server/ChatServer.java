package server;

import java.util.ArrayList;
import java.util.List;

public class ChatServer {
    private List<ChatClientStub> clients;

    public ChatServer() {
        this.clients = new ArrayList<ChatClientStub>();
    }

    public void register(ChatClientStub client) {
        clients.add(client);
        clients.forEach(c -> c.sendMessage(client.getName() + " enterd the room"));
    }

    public void unregister(ChatClientStub client) {
        clients.remove(client);
        clients.forEach(c -> c.sendMessage(client.getName() + " left the room"));
    }

    public void send(String name, String message) {
        for(ChatClientStub client : clients) {
            clients.forEach(c -> c.sendMessage(client.getName() + " : " + message));
        }
    }
}
