package server;

import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public class ChatServerSkeleton {
    private final MessageManager messageManager;
    private final ChatServers chatServers;

    public ChatServerSkeleton() {
        messageManager = new MessageManager();
        System.out.println("my address = " + messageManager.getMyAddress());
        chatServers = new ChatServerImpl();
    }

    private void handleRegister(MethodCallMessage request){
        chatServers.register(new ChatClientStub(new NetworkAddress(request.getParameter("ipAddress"), Integer.parseInt(request.getParameter("port")))));
        sendEmptyReply(request);
    }

    private void handeUnRegister(MethodCallMessage request){
        chatServers.unRegister(new ChatClientStub(new NetworkAddress(request.getParameter("ipAddress"), Integer.parseInt(request.getParameter("port")))));
        sendEmptyReply(request);
    }

    private void handleSend(MethodCallMessage request){
        chatServers.send(request.getParameter("name"), request.getParameter("message"));
        sendEmptyReply(request);
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void run(){
        while (true){
            MethodCallMessage req = messageManager.wReceive();
            handleRequest(req);
        }
    }

    private void handleRequest(MethodCallMessage request) {
        String methodName = request.getMethodName();
        if ("register".equals(methodName)) {
            handleRegister(request);
        }else if ("unRegister".equals(methodName)){
            handeUnRegister(request);
        }else if ("send".equals(methodName)){
            handleSend(request);
        }
        else {
            System.out.println("ContactsSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    public static void main(String[] args) {
        ChatServerSkeleton chatServerSkeleton = new ChatServerSkeleton();
        chatServerSkeleton.run();
    }
}
