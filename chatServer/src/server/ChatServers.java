package server;

public interface ChatServers {
    void send(String name, String Message);
    void register(ChatClientStub chatClientStub);
    void unRegister(ChatClientStub chatClientStub);
}
