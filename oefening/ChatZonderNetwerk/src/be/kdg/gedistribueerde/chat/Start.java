package be.kdg.gedistribueerde.chat;

public class Start {
    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer();

        ChatClient chatClient1 = new ChatClient(chatServer, "ikke");
        new ChatFrame(chatClient1);
        chatClient1.register();

        ChatClient chatClient2 = new ChatClient(chatServer, "bla");
        new ChatFrame(chatClient2);
        chatClient2.register();
    }
}
