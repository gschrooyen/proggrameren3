package be.kdg.prog4.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BasisReflection {
    private static MijnClass mijnClassObject;

    public static void main(String[] args) throws NoSuchFieldException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class myClass = MijnClass.class;
        mijnClassObject = new MijnClass("bla");
        System.out.println(mijnClassObject.getClass().equals(myClass));
        System.out.println();

        Field field = myClass.getDeclaredField("attrib");
        printField(field);
        System.out.println();

        Method methods[] = myClass.getDeclaredMethods();
        printMethod(methods[0]);
        System.out.println();

        mijnClassObject = (MijnClass) myClass.getDeclaredConstructor(String.class).newInstance("attrib");
        System.out.println("myClassObject.getAttrib() = " + mijnClassObject.getAttrib(0));
    }

    private static void printMethod(Method method) throws InvocationTargetException, IllegalAccessException {
        System.out.println("***** Method *****");
        System.out.println("method.getName() = " + method.getName());
        Class parameterTypes[] = method.getParameterTypes();
        for(Class c : parameterTypes) {
            System.out.println("parametertype " + c.getName());
        }
        System.out.println("method.getReturnType() = " + method.getReturnType());
        Object returnValue = method.invoke(mijnClassObject, 3);
        System.out.println("returnValue = " + returnValue);
    }

    private static void printField(Field field) throws IllegalAccessException {
        System.out.println("***** Field *****");
        System.out.println("field.getName() = " + field.getName());
        System.out.println("field.getType() = " + field.getType());
        field.setAccessible(true);
        System.out.println("field.get(myClassObject) = " + field.get(mijnClassObject));
        field.set(mijnClassObject, "haha");
        System.out.println("myClassObject.getAttrib() = " + mijnClassObject.getAttrib(0));
    }
}
