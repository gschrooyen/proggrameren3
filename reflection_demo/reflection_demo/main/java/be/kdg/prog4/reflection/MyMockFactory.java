package be.kdg.prog4.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class MyMockFactory {
    public static Object createMock(Class myClass) {
        InvocationHandler handler = new MyInvocationHandler();
        Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{myClass}, handler);
        return result;
    }
}
