package be.kdg.prog4.reflection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyXmlReader {
    public static Object read(InputStream stream) throws ParserConfigurationException, IOException, SAXException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        Document document = documentBuilder.parse(stream);

        Element rootElement = document.getDocumentElement();
        String className = rootElement.getTagName();
        //System.out.println("className = " + className);

        NodeList packageNodes = rootElement.getElementsByTagName("package");
        Element packageElement = (Element) packageNodes.item(0);
        String packageName = packageElement.getTextContent();
        //System.out.println("packageName = " + packageName);

        Class objectClass = Class.forName(packageName+"."+className);
        Object object = objectClass.getDeclaredConstructor().newInstance();

        Element fieldsNode = (Element) rootElement.getElementsByTagName("fields").item(0);
        NodeList fieldNodes = fieldsNode.getElementsByTagName("field");
        for(int i=0; i<fieldNodes.getLength(); i++) {
            Element fieldElement = (Element) fieldNodes.item(i);
            Element nameElement = (Element) (fieldElement.getElementsByTagName("name").item(0));
            Element valueElement = (Element) (fieldElement.getElementsByTagName("value").item(0));
            String name = nameElement.getTextContent();
            String value = valueElement.getTextContent();
            System.out.println("name = " + name);
            System.out.println("value = " + value);
            //String setterName = "set" + name.substring(0,1).toUpperCase()+name.substring(1,name.length());
            //Method method = null;
            //try {
            //    method = objectClass.getMethod(setterName, new Class[] { String.class });
            //    method.invoke(object, value);
            //} catch (NoSuchMethodException e) {
            //    method = objectClass.getMethod(setterName, new Class[] { int.class });
            //    method.invoke(object, Integer.parseInt(value));
            //}
            try {
                Field field = objectClass.getDeclaredField(name);
                field.setAccessible(true);
                if (field.getType().equals(String.class)) {
                    field.set(object, value);
                } else {
                    field.setInt(object, Integer.parseInt(value));
                }
            } catch (NoSuchFieldException e) {
                System.err.println("Cannot find field " + name);
            }
        }

        return object;
    }
}
