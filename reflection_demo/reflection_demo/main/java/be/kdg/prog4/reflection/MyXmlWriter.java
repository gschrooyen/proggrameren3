package be.kdg.prog4.reflection;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyXmlWriter {
    public static void write(Object object, PrintStream stream) throws FileNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        stream.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

        Class objectClass = object.getClass();
        String className = objectClass.getSimpleName();
        stream.println("<" + className + ">");

        stream.println("\t<package>" + objectClass.getPackage().getName() + "</package>");

        Annotation[] annotations = objectClass.getAnnotations();
        for(Annotation annotation : annotations) {
            stream.println("\t<annotation>");
            stream.println("\t\t<type>" + annotation + "</type>");
            stream.println("\t</annotation>");
        }

        stream.println("\t<fields>");
        Field[] fields = objectClass.getDeclaredFields();
        for(Field field : fields) {
            String fieldName = field.getName();
            // methode 1
            //String getter = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
            //Method getterMethod = objectClass.getMethod(getter);
            //String fieldValue = getterMethod.invoke(object).toString();
            // methode 2
            field.setAccessible(true);
            String fieldValue = field.get(object).toString();

            stream.println("\t\t<field>");
            stream.println("\t\t\t<name>" + fieldName + "</name>");
            stream.println("\t\t\t<value>" + fieldValue + "</value>");
            stream.println("\t\t</field>");
        }
        stream.println("\t</fields>");

        stream.println("</" + className + ">");
    }
}
