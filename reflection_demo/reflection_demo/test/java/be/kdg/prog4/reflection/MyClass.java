package be.kdg.prog4.reflection;

import java.util.Objects;

@Deprecated
public class MyClass {
    private int a;
    private String b;

    public MyClass() {
        this.a = 3;
        this.b = "bla";
    }

    public MyClass(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public String getB() {
        return b;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(String b) {
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyClass myClass = (MyClass) o;
        return a == myClass.a &&
                Objects.equals(b, myClass.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "a=" + a +
                ", b='" + b + '\'' +
                '}';
    }
}
