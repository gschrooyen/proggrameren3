package be.kdg.prog4.reflection;

public interface MyInterface {
    public int getA();
    public String getB();
}
