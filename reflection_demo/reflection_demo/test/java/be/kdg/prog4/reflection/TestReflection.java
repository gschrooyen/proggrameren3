package be.kdg.prog4.reflection;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Observable;
import java.util.Observer;

import static org.junit.Assert.assertEquals;

public class TestReflection {
    @Test
    public void testWriteXml() throws IOException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, ParserConfigurationException, InstantiationException, SAXException {
        //PrintStream writer = new PrintStream(new FileOutputStream("myObject.xml"));
        MyClass myObject = new MyClass(5, "haha");
        MyXmlWriter.write(myObject, System.out);
        //writer.close();

        InputStream stream = new FileInputStream("myObject.xml");
        myObject = (MyClass) MyXmlReader.read(stream);
        stream.close();
        MyClass expected = new MyClass(5, "haha");

        assertEquals(expected, myObject);
    }

    @Test
    public void testCreateMock() {
        MyInterface mock = (MyInterface) MyMockFactory.createMock(MyInterface.class);
        int a = mock.getA();
        assertEquals(-1, a);
        String b = mock.getB();
        assertEquals("hihi", b);
    }

    @Test
    public void testOtherMock() {
        Observer myObserver = (Observer) MyMockFactory.createMock(Observer.class);
        myObserver.update(new Observable(), "hoihoi");
    }
}
