package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler implements InvocationHandler {

    private String address;
    private int port;
    private MessageManager messageManager;

    MyInvocationHandler(String address, int port) {
        this.address = address;
        this.port = port;
        messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), method.getName());
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                System.out.println("args[i].getClass() = " + args[i].getClass());
                if (checkPrimitive(args[i].getClass())) {
                    message.setParameter("arg" + String.valueOf(i), args[i].toString());
                    System.out.println("nam = " + "arg" + String.valueOf(i));
                    System.out.println("value = " + args[i].toString());
                } else {
                    for (int j = 0; j < args[i].getClass().getDeclaredFields().length; j++) {
                        Field f = args[i].getClass().getDeclaredFields()[j];
                        f.setAccessible(true);
                        message.setParameter("arg" + String.valueOf(i) + "." + f.getName(), String.valueOf(f.get(args[i])));
                    }
                }
            }
        }
        messageManager.send(message, new NetworkAddress(address, port));
        Class type = method.getReturnType();
        MethodCallMessage msg = messageManager.wReceive();
        Object out = msg.getParameter("result");
        if (type == int.class){
            out = Integer.parseInt(out.toString());
        }else if(type == char.class){
            out = ((String) out).toCharArray()[0];
        }else if(type == boolean.class) {
            out = Boolean.valueOf(out.toString());
        }else if(! checkPrimitive(type)){
            Object workingObj = type.getDeclaredConstructor().newInstance();
            for (Field field:
                 workingObj.getClass().getDeclaredFields()) {
                System.out.println("attName = " + field.getName());
                field.setAccessible(true);
                switch (field.getType()){
                    // TODO: 12/12/2019 omzetten van elk attr naar de juiste type
                }
                field.set(workingObj, msg.getParameter("result." + field.getName()));
            }
        }
        return out;
    }

    private boolean checkPrimitive(Class obj){
        return (obj.isPrimitive() || obj == String.class || obj == Integer.class || obj == Boolean.class || obj == Double.class || obj == Character.class);
    }
}
