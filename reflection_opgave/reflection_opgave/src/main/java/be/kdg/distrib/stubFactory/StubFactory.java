package be.kdg.distrib.stubFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

public class StubFactory {
    public static <T> T createStub(Class<T> interf, String adress, int port) {
        InvocationHandler handler = new MyInvocationHandler(adress, port);
        T obj = (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{interf}, handler);
        return obj;
    }
}
