package be.kdg.prog4.spel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Spel {
    private Dobbelsteen dobbelsteen;
    private int score;

    public void Spel() {
       score = -1;
    }

    @Autowired
    public void setDobbelsteen(Dobbelsteen dobbelsteen) {
        this.dobbelsteen = dobbelsteen;
    }

    public int getScore() {
        return score;
    }

    public void beginSpel() {
        score = 0;
    }

    public void rolDobbelsteen() {
        dobbelsteen.rol();
    }

    public void berekenScore() {
        if (dobbelsteen.getWaarde() == 6) {
            score++;
        }
    }
}
