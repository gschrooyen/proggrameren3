package be.kdg.prog4.spel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"context.xml"})
public class TestSpel {
    @Autowired
    private Spel spel;

    @Before
    public void init() {
        spel = new Spel();
        Dobbelsteen testDobbelsteen = mock(Dobbelsteen.class);
        spel.setDobbelsteen(testDobbelsteen);
        when(testDobbelsteen.getWaarde()).thenReturn(1).thenReturn(2).thenReturn(6);
    }

    @Test
    public void testSpel() {
        spel.beginSpel();
        for (int i = 0; i < 3; i++) {
            spel.rolDobbelsteen();
            spel.berekenScore();
        }
        int score = spel.getScore();
        assertEquals(1, score);
    }
}
