package be.kdg.prog4.tdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FavoriteService {
    @Autowired
    private UserManagement userManagement;


    public void addUser(String root, String rootpasswd, String newUsername, String newPassword) {
        if (checkRoot(root, rootpasswd)) {
            userManagement.addUser(new User(newUsername, newPassword));
        }
    }

    public void removeUser(String root, String rootpasswd, String username, String password) {
        if (checkRoot(root, rootpasswd)) {
            userManagement.removeUser(new User(username, password));
        }
    }

    public boolean checkLogin(String username, String password) {
        return userManagement.getUsers().contains(new User(username, password));
    }

    private boolean checkRoot(String username, String password) {
        return "root".equals(username) && "rootpasswd".equals(password);
    }

    public List<String> getFavorites(String username, String password) {
        User usr = userManagement.getUser(username, password);
        if (usr != null) {
            return usr.getFavorites();
        }
        return new ArrayList<>();
    }

    public void addFavorite(String username, String password, String favorite) {
        User usr = userManagement.getUser(username, password);
        if (usr != null) {
            usr.getFavorites().add(favorite);
        }
    }

    public void removeFavorite(String username, String password, String favorite) {
        User usr = userManagement.getUser(username, password);
        if (usr != null){
            usr.getFavorites().remove(favorite);
        }

    }

}
